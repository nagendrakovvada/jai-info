from rest_framework import routers
from .views import *


emp_router = routers.DefaultRouter()
emp_router.register(r'emp', EmpViewSet)
