from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework.viewsets import ModelViewSet
# Create your views here.
class EmpViewSet(ModelViewSet):
    queryset = Emp_data.objects.all()
    serializer_class = EmpSerializer