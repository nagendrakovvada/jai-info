# Generated by Django 3.1.7 on 2021-03-07 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jaiinfoapp', '0002_auto_20210307_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emp_data',
            name='empId',
            field=models.CharField(max_length=20, unique=True),
        ),
    ]
