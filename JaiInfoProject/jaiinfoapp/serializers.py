from rest_framework import serializers
from .models import *

class EmpSerializer(serializers.ModelSerializer):
    class Meta:
        model = Emp_data
        fields = "__all__"