from django.db import models

# Create your models here.
class Emp_data(models.Model):
	empId		= models.CharField(max_length=20,unique=True)
	name		= models.CharField(max_length=70)
	job_title	= models.CharField(max_length=100)
	Salary		= models.IntegerField()
	def __str__(self):
		return self.empId,self.name,self.job_title